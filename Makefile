include src/shared/common.mk
.ONESHELL:
SHELL := bash
.SHELLFLAGS = -euo pipefail -c
PROJECT_ID := 57777019
ARTIFACTORY_DEPLOY_URL := https://gitlab.com/api/v4/projects/$(PROJECT_ID)/packages/maven
JAVA_VERSION := 21

auth-java: auth-spring auth-feign

auth-spring:
	$(MAKE) docker-build VARIANT=spring MODULE=auth EXTRA_DEPS='$(SHARED_DIR)/common.yml'
	$(MAKE) install-java VARIANT=spring MODULE=auth EXTRA_DEPS='$(SHARED_DIR)/common.yml'

auth-feign:
	$(MAKE) docker-build VARIANT=feign MODULE=auth EXTRA_DEPS='$(SHARED_DIR)/common.yml'
	$(MAKE) install-java VARIANT=feign MODULE=auth EXTRA_DEPS='$(SHARED_DIR)/common.yml'

auth-ts:
	$(MAKE) docker-build VARIANT=typescript MODULE=auth EXTRA_DEPS='$(SHARED_DIR)/common.yml'

auth-lint:
	$(MAKE) docker-lint MODULE=auth EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

comments-java: comments-spring comments-feign

comments-spring:
	$(MAKE) docker-build VARIANT=spring MODULE=comments EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'
	$(MAKE) install-java VARIANT=spring MODULE=comments EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

comments-feign:
	$(MAKE) docker-build VARIANT=feign MODULE=comments EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'
	$(MAKE) install-java VARIANT=feign MODULE=comments EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

comments-ts:
	$(MAKE) docker-build VARIANT=typescript MODULE=comments EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

comments-lint:
	$(MAKE) docker-lint MODULE=comments EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

dictionary-java: dictionary-spring dictionary-feign

dictionary-spring:
	$(MAKE) docker-build VARIANT=spring MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'
	$(MAKE) install-java VARIANT=spring MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

dictionary-feign:
	$(MAKE) docker-build VARIANT=feign MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'
	$(MAKE) install-java VARIANT=feign MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

dictionary-ts:
	$(MAKE) docker-build VARIANT=typescript MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

dictionary-lint:
	$(MAKE) docker-lint MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'

user-java: user-spring user-feign

user-spring:
	$(MAKE) docker-build VARIANT=spring MODULE=user EXTRA_DEPS='$(SHARED_DIR)/common.yml'
	$(MAKE) install-java VARIANT=spring MODULE=user EXTRA_DEPS='$(SHARED_DIR)/common.yml'

user-feign:
	$(MAKE) docker-build VARIANT=feign MODULE=user EXTRA_DEPS='$(SHARED_DIR)/common.yml'
	$(MAKE) install-java VARIANT=feign MODULE=user EXTRA_DEPS='$(SHARED_DIR)/common.yml'

user-ts:
	$(MAKE) docker-build VARIANT=typescript MODULE=user EXTRA_DEPS='$(SHARED_DIR)/common.yml'

user-lint:
	$(MAKE) docker-lint MODULE=user EXTRA_DEPS='$(SHARED_DIR)/common.yml $(SHARED_DIR)/stack.yml'
